#include "AlgorithmX.h"

#include <algorithm>
#include <cctype>

#include <iostream>

bool AlgorithmX::solve() {
    // choose column, deterministically
    if (constraints.empty()) { return true; }
    auto columnIterator = chooseColumn();
    auto chosenColumn = *columnIterator;
    for (auto nonzeroRow : chosenColumn) {
        // filter rows to ignore
        if (rowsToIgnore.count(nonzeroRow) != 0) { continue; }

        auto additionalRowsToIgnore = std::vector<size_t>{};
        auto columnsToIgnore = std::vector<Column>{};
        _solution.emplace_back(nonzeroRow);
        for (auto&& column : constraints) {
            if (column.count(_solution.back()) != 0) {
                // add all rows in column to the list of additional rows to ignore
                for (auto row : column) {
                    auto iteratorSuccess = rowsToIgnore.emplace(row);
                    if (iteratorSuccess.second) { 
                        additionalRowsToIgnore.emplace_back(row);
                    }
                }
                // move columns from matrix to the list of columns to ignore
                columnsToIgnore.emplace_back(column);
            }
        }
        // eliminate columns and rows
        constraints.eraseSorted(begin(columnsToIgnore), end(columnsToIgnore));
        if (solve()) { return true; }
        // else, we have to restore the previous state
        _solution.pop_back();
        for (auto r : additionalRowsToIgnore) { rowsToIgnore.erase(r); }
        constraints.insertSorted(begin(columnsToIgnore), end(columnsToIgnore));
    }
    return false;
}

typename AlgorithmX::Matrix::const_iterator AlgorithmX::chooseColumn() const {
    return std::min_element(begin(constraints), end(constraints), [](auto&& lhs, auto&& rhs) { return lhs.size() < rhs.size(); });
}
