#include <iostream>
#include <fstream>

#include <chrono>

#include "AlgorithmX.h"
#include "SudokuXConstraints.h"

#include "ExactCoverSolver.h"

int main(int argc, char* argv[]) {
	// test matrix:
	// 0 1 0 0
	// 1 1 0 0
	// 0 0 1 0
	// 0 1 1 1
	// 0 0 0 1
	auto constraints = std::vector<std::vector<size_t>>{
		{1}, {0, 1, 3}, {2, 3}, {3, 4}
	};
	auto exactCoverSolver = ExactCoverSolver::fromConstraintMatrix(constraints);
	auto solution = exactCoverSolver.solve();
	for (auto row : solution) {
		std::cout << row << ' ';
	}
	std::cout << '\n';

    if (argc == 1 || "-h" == argv[1] || "--help" == argv[1]) {
        std::cout << "simply provide the path to a filename containing a sudoku to solve\n\n"
                  << "the file must have at least 10 lines, the first 9 lines containing\n"
                  << "the numbers (or any other char like '.' or ' ' as placeholder) and\n"
                  << "no additional separator.\n";
        return 0;
    }

    int nFails = 0;

    for (auto i = 1; i!=argc; ++i) {
        auto stream = std::ifstream(argv[i]);
        if (! stream.good()) {
            std::cout << "failed to open file " << argv[i] << '\n';
            --nFails;
            continue;
        }

        using Clock = std::chrono::steady_clock;
        auto startRead = Clock::now();
        auto rows = AlgorithmX::Solution{};
        try {
            rows = readInitialSudoku(stream);
        }
        catch (std::runtime_error& e) {
            std::cerr << "failed to read file " << argv[1] << '\n';
			std::cerr << e.what() << '\n';
        }
        auto readTook = Clock::now() - startRead;
        
        auto startSetup = Clock::now();
        auto mat = setupConstraints(rows);
        auto setupTook = Clock::now() - startSetup;

		auto otherMat = std::vector<std::vector<ExactCoverSolver::Row>>{};
		std::transform(begin(mat), end(mat), std::back_inserter(otherMat),
			[](auto&& s) { 
				return std::vector<ExactCoverSolver::Row>(begin(s), end(s)); 
			});

        auto solver = AlgorithmX(mat);
        auto startSolver = Clock::now();
        auto solveResult = solver.solve();
        auto solverTook = Clock::now() - startSolver;

		auto otherSolver = ExactCoverSolver::fromConstraintMatrix(otherMat);
		auto startOtherSolver = Clock::now();
		auto solveOtherResult = otherSolver.solve();
		auto otherSolverTook = Clock::now() - startOtherSolver;

		if (solveResult) {
			auto solvedSudoku = solver.solution();
			solvedSudoku.insert(end(solvedSudoku), begin(rows), end(rows));
			outputSudoku(solvedSudoku, std::cout);
		}
		else {
			outputSudoku(rows, std::cerr << "failed to solve sudoku:\n");
		}

		if (! solveOtherResult.empty()) {
			solveOtherResult.insert(end(solveOtherResult), begin(rows), end(rows));
			outputSudoku(solveOtherResult, std::cout << '\n');
		}
		else {
			outputSudoku(rows, std::cerr << "failed to solve sudoku:\n");
		}
        std::cerr << "\nreading: " << std::chrono::duration_cast<std::chrono::microseconds>(readTook).count()
                  << " microseconds\nconstraints: " << std::chrono::duration_cast<std::chrono::microseconds>(setupTook).count()
                  << " microseconds\nsolver: " << std::chrono::duration_cast<std::chrono::microseconds>(solverTook).count()
                  << " microseconds\nother solver: " << std::chrono::duration_cast<std::chrono::microseconds>(otherSolverTook).count()
				  << " microseconds\n\n";
    }

    return nFails;
}
