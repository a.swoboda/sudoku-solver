#include "ExactCoverSolver.h"

#include <algorithm>

ExactCoverSolver::ExactCoverSolver(Matrix m)
	: matrix(std::move(m))
{
	for (auto&& col : matrix) {
		std::sort(begin(col.data), end(col.data));
	}
}

ExactCoverSolver::Solution ExactCoverSolver::solve() {
	for (auto chosenColumn = chooseColumn();
		end(matrix) != chosenColumn;
		chosenColumn = chooseColumn()) {
		addChosenColum(chosenColumn);
		auto rowIt = firstRow();
		while (end(chosenCols.back()->data) == rowIt) {
			if (!canRewind()) {  // can't rewind
				return {};
			}
			auto prevRow = rewind();
			rowIt = nextRow(prevRow);
		}
		chosenRows.emplace_back(*rowIt);
		removeConstraintsAndRows(*rowIt);
	}
	return chosenRows;
}

ExactCoverSolver::ConstraintIterator ExactCoverSolver::chooseColumn() const
{
	auto it = std::find_if(begin(matrix),
		end(matrix),
		[](auto&& c) { return !c.ignored; });
	return std::min_element(it,
		end(matrix),
		[](auto&& lhs, auto&& rhs) {
			static auto const mapper = [](Column const& c) {
				return c.ignored
					? std::numeric_limits<Row>::max()
					: c.data.size();
			};
			return mapper(lhs) < mapper(rhs);
		});
}

void ExactCoverSolver::addChosenColum(ConstraintIterator colIt)
{
	chosenCols.emplace_back(colIt);
}

ExactCoverSolver::ColumnIterator ExactCoverSolver::firstRow() const
{
	auto&& col = chosenCols.back()->data;
	return findFirstNotInSet(begin(col),
		end(col),
		begin(sortedIgnoredRows),
		end(sortedIgnoredRows));
}

ExactCoverSolver::ColumnIterator ExactCoverSolver::nextRow(Row row) const
{
	auto&& col = chosenCols.back()->data;
	auto it = std::lower_bound(begin(col), end(col), row);
	++it;
	return findFirstNotInSet(it,
		end(col),
		begin(sortedIgnoredRows),
		end(sortedIgnoredRows));
}

ExactCoverSolver::Row ExactCoverSolver::rewind()
{
	chosenCols.back()->ignored = false;
	chosenCols.pop_back();
	for (auto it : colsToRewind.back()) {
		it->ignored = false;
	}
	colsToRewind.pop_back();

	sortedIgnoredRows.eraseSorted(begin(rowsToRewind.back()),
		end(rowsToRewind.back()));
	rowsToRewind.pop_back();

	auto result = chosenRows.back();
	chosenRows.pop_back();
	return result;
}

void ExactCoverSolver::removeConstraintsAndRows(Row row)
{
	auto additonalRowsToIgnore = std::vector<Row>{};
	auto additionalColsToIgnore = std::vector<ConstraintIterator>{};
	for (auto it = begin(matrix); end(matrix) != it; ++it) {
		auto&& col = *it;
		if (!col.ignored
			&& std::binary_search(begin(col.data), end(col.data), row)) {
			col.ignored = true;
			additionalColsToIgnore.emplace_back(it);
			auto offset = additonalRowsToIgnore.size();
			auto it = std::set_difference(begin(col.data),
				end(col.data),
				begin(sortedIgnoredRows),
				end(sortedIgnoredRows),
				std::back_inserter(additonalRowsToIgnore));
			sortedIgnoredRows.insertSorted(
				begin(additonalRowsToIgnore) + offset,
				end(additonalRowsToIgnore));
		}
	}
	colsToRewind.emplace_back(std::move(additionalColsToIgnore));
	chosenCols.back()->ignored = true;
	std::sort(begin(additonalRowsToIgnore), end(additonalRowsToIgnore));
	rowsToRewind.emplace_back(std::move(additonalRowsToIgnore));
}
