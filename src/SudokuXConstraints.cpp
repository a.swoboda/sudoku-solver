#include "SudokuXConstraints.h"

#include <algorithm>
#include <istream>

#include "SudokuX.h"

AlgorithmX::Column computeRowColConstraint(size_t row, size_t col) {
    auto result = AlgorithmX::Column{};
    for (size_t value = 0; SudokuX::nValues != value; ++value) {
        result.emplace(SudokuX::rowFrom(row, col, value));
    }
    return result;
}

AlgorithmX::Matrix computeRowColConstraints() {
    AlgorithmX::Matrix result{};
    for (size_t row = 0; SudokuX::nRows != row; ++row) {
        for (size_t col = 0; SudokuX::nCols != col; ++col) {
            result.emplace(computeRowColConstraint(row, col));
        }
    }
    return result;
}

AlgorithmX::Column computeRowNumConstraint(size_t row, size_t num) {
    auto result = AlgorithmX::Column{};
    for (size_t col = 0; SudokuX::nCols != col; ++col) {
        result.emplace(SudokuX::rowFrom(row, col, num));
    }
    return result;
}

AlgorithmX::Matrix computeRowNumConstraints() {
    AlgorithmX::Matrix result{};
    for (size_t row = 0; SudokuX::nRows != row; ++row) {
        for (size_t value = 0; SudokuX::nValues != value; ++value) {
            result.emplace(computeRowNumConstraint(row, value));
        }
    }
    return result;
}

AlgorithmX::Column computeColNumConstraint(size_t col, size_t num) {
    auto result = AlgorithmX::Column{};
    for (size_t row = 0; SudokuX::nRows != row; ++row) {
        result.emplace(SudokuX::rowFrom(row, col, num));
    }
    return result;
}

AlgorithmX::Matrix computeColNumConstraints() {
    AlgorithmX::Matrix result{};
    for (size_t col = 0; SudokuX::nCols != col; ++col) {
        for (size_t value = 0; SudokuX::nValues != value; ++value) {
            result.emplace(computeColNumConstraint(col, value));
        }
    }
    return result;
}

AlgorithmX::Column computeBoxNumConstraint(size_t blockRow, size_t blockCol, size_t num) {
    auto result = AlgorithmX::Column{};
    for (size_t row = blockRow*SudokuX::blockHeight; (blockRow+1)*SudokuX::blockHeight != row; ++row) {
        for (size_t col = blockCol*SudokuX::blockWidth; (blockCol+1)*SudokuX::blockWidth != col; ++col) {
            result.emplace(SudokuX::rowFrom(row, col, num));
        }
    }
    return result;
}

AlgorithmX::Matrix computeBoxNumConstraints() {
    AlgorithmX::Matrix result{};
    for (size_t blockRow = 0; SudokuX::nBlockRows != blockRow; ++blockRow) {
        for (size_t blockCol = 0; SudokuX::nBlockCols != blockCol; ++blockCol) {
            for (size_t num = 0; SudokuX::nValues != num; ++num) {
                result.emplace(computeBoxNumConstraint(blockRow, blockCol, num));
            }
        }
    }
    return result;
}

AlgorithmX::Matrix computeSudokuMatrix() {
    auto solution = computeRowColConstraints();
    auto temp = computeRowNumConstraints();
    solution.insert(begin(temp), end(temp));
    temp = computeColNumConstraints();
    solution.insert(begin(temp), end(temp));
    temp = computeBoxNumConstraints();
    solution.insert(begin(temp), end(temp));
    if (4*9*9 != solution.size()) { throw std::runtime_error("wrong number of columns"); }
    return solution;
}

AlgorithmX::Matrix sudokuMatrix() {
    static auto const result = computeSudokuMatrix();
    
    return result;
}

AlgorithmX::Solution readInitialSudoku(std::istream& stream) {
    auto result = AlgorithmX::Solution{};

    for (size_t i = 0; SudokuX::nRows != i; ++i) {
        for (size_t j = 0; SudokuX::nCols != j; ++j) {
            int c = stream.get();
            if (SudokuX::isSudokuDigit(c)) {
                auto num = c - static_cast<size_t>('1');
                result.emplace_back(SudokuX::rowFrom(i, j, num));
            }
        }
        auto const nl = stream.get();
        if ('\n' != nl) { throw std::runtime_error("expected a newline character"); }
    }
    return result;
}

AlgorithmX::Matrix setupConstraints(AlgorithmX::Solution initialRows) {
    std::sort(begin(initialRows), end(initialRows));
    auto allConstraints = sudokuMatrix();
    auto keptConstraints = AlgorithmX::Matrix();
    auto discardedRows = AlgorithmX::Column{};
    for (auto&& col : allConstraints) {
        if (col.overlapsSorted(begin(initialRows), end(initialRows))) {
            discardedRows.insert(begin(col), end(col));  // TODO discardedRows.merge(col)
        }
        else {
            keptConstraints.emplace(col);
        }
    }
    // remove discarded rows from constraints
    auto result = AlgorithmX::Matrix{};
    for (auto col : keptConstraints) {
        col.eraseSorted(begin(discardedRows), end(discardedRows));
        if (! col.empty()) {
            result.emplace(std::move(col));
        }
    }
    return result;
}

std::array<std::array<size_t, 9>, 9> rowsToMatrix(AlgorithmX::Solution const& solution) {
    constexpr size_t ldim = 9, rdim = 9, nValues = 9;
    std::array<std::array<size_t, rdim>, ldim> result{};
    for (auto row : solution) {
        auto i = row / (rdim*nValues);
        auto j = (row / nValues) % rdim;
        auto value = row % nValues;
        result[i][j] = value+1;
    }
    return result;
}

void outputSudoku(AlgorithmX::Solution const& solution, std::ostream& out) {
    auto asSudoku = std::array<std::array<size_t, SudokuX::nCols>, SudokuX::nRows>{};
    for (auto row : solution) {
        auto i = SudokuX::rowColNumFromRow(row);
        asSudoku[i.row][i.col] = i.num+1;
    }
    for (auto&& row : asSudoku) {
        for (auto e : row) {
            out << e;
        }
        out << '\n';
    }
}
