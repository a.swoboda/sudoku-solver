#ifndef SUDOKUX_SUDOKUXCONSTRAINTS_H
#define SUDOKUX_SUDOKUXCONSTRAINTS_H

#include <array>

#include "AlgorithmX.h"

AlgorithmX::Solution readInitialSudoku(std::istream& stream);

AlgorithmX::Matrix setupConstraints(AlgorithmX::Solution initialRows);

void outputSudoku(AlgorithmX::Solution const& solution, std::ostream& out);

// --- below are helpers ---

AlgorithmX::Matrix sudokuMatrix();  // containing all possible sudoku constraints

AlgorithmX::Column computeRowColConstraint(size_t row, size_t col);

AlgorithmX::Matrix computeRowColConstraints();

AlgorithmX::Column computeRowNumConstraint(size_t row, size_t num);

AlgorithmX::Matrix computeRowNumConstraints();

AlgorithmX::Column computeColNumConstraint(size_t col, size_t num);

AlgorithmX::Matrix computeColNumConstraints();

AlgorithmX::Column computeBoxNumConstraint(size_t blockRow, size_t blockCol, size_t num);

AlgorithmX::Matrix computeBoxNumConstraints();

AlgorithmX::Matrix computeSudokuMatrix();

std::array<std::array<size_t, 9>, 9> rowsToMatrix(AlgorithmX::Solution const& solution);

#endif  // SUDOKUX_SUDOKUXCONSTRAINTS_H
