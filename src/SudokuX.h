#ifndef SUDOKUX_SUDOKUX_H
#define SUDOKUX_SUDOKUX_H

#include <cstddef>

struct SudokuX {
    static constexpr size_t nRows = 9, nCols = 9, nValues = 9;
    static constexpr size_t nBlockRows = 3, nBlockCols = 3;
    static constexpr size_t blockHeight = 3, blockWidth = 3;

    static size_t rowFrom(size_t row, size_t column, size_t value) {
        // It is the callers job to make sure that the values provided are smaller than the respective dimensions
        return row * nCols * nValues + column * nValues + value;
    }

    static auto rowColNumFromRow(size_t s) {
        struct Result { size_t row, col, num; } result;
        // auto result = std::array<size_t, 3>{};
        result.row = s / (nCols * nValues);
        s %= (nCols * nValues);
        result.col = s / nValues;
        result.num = s % nValues;
        return result;
    }

    static constexpr size_t blockRowFrom(size_t row) {
        return row / blockHeight;
    }

    static constexpr size_t blockColFrom(size_t col) {
        return col / blockWidth;
    }

    static auto isSudokuDigit(int c) { return isdigit(c) && '0' != c; };
};

#endif  // SUDOKUX_SUDOKUX_H
