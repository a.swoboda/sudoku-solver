#ifndef SUDOKUX_ALGORITHMX_H
#define SUDOKUX_ALGORITHMX_H

#include <vector>

#include "flat_set.h"

struct AlgorithmX {
    using Column = FlatSet<size_t>;
    using Matrix = FlatSet<Column>;
    using Solution = std::vector<size_t>;

    Solution solution() const { return _solution; }

    AlgorithmX(Matrix m) : constraints(std::move(m)) {}
    
    bool solve();

    typename Matrix::const_iterator chooseColumn() const;

private:
    Matrix constraints = {};

    FlatSet<size_t> rowsToIgnore = {};

    std::vector<size_t> _solution = {};
};

#endif  // SUDOKUX_ALGORITHMX_H
