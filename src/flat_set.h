#ifndef SUDOKUX_FLATSET_H
#define SUDOKUX_FLATSET_H

#include <algorithm>
#include <utility>
#include <vector>

template <typename T>  // TODO comparator, allocator
class FlatSet {
private:
    std::vector<T> impl;

    friend bool operator <(FlatSet const& lhs, FlatSet const& rhs) {
        return lhs.impl < rhs.impl;
    }

    friend auto begin(FlatSet const& s) { return s.begin(); }
    friend auto end(FlatSet const& s) { return s.end(); }

    friend auto operator==(FlatSet const& lhs, FlatSet const& rhs) {
        return lhs.impl == rhs.impl;
    }
    friend auto operator!=(FlatSet const& lhs, FlatSet const& rhs) {
        return lhs.impl != rhs.impl;    
    }

public: 
    size_t count(T const& t) const {
        return std::binary_search(impl.begin(), impl.end(), t) ? 1 : 0;
    }

    using const_iterator = typename decltype(impl)::const_iterator;

    template <typename...Args>
    std::pair<const_iterator, bool> emplace(Args&&...args) {
        auto tmp = T(std::forward<Args>(args)...);
        auto it = std::lower_bound(impl.begin(), impl.end(), tmp);
        if (impl.end() == it || tmp != *it) {
            impl.emplace(it, std::move(tmp));
            return {it, true};
        }
        else {
            return {it, false};
        }
    }

    template <typename Iterator>
    void insert(Iterator from, Iterator to) {
        while (from != to) {
            emplace(*from++);
        }
    }

    size_t erase(T const& t) {
        auto it = std::lower_bound(impl.begin(), impl.end(), t);
        if (impl.end() != it && t == *it) {
            impl.erase(it);
            return 1;
        }
        return 0;
    }

    auto erase(const_iterator it) {
        return impl.erase(it);
    }

    auto empty() const { return impl.empty(); }

    auto size() const { return impl.size(); }

    auto begin() const { return impl.begin(); }
    auto end() const { return impl.end(); }

    template <typename Iterator>
    bool overlapsSorted(Iterator from, Iterator to) const {
        for (auto i = begin(), e = end(); i != e && from != to; ) {
            if (*i < *from) { ++i; }
            else if (*from < *i) { ++from; }
            else { return true; }
        }
        return false;
    }

    template <typename Iterator>
    size_t eraseSorted(Iterator from, Iterator to) {
        size_t result = 0;
        auto i = impl.begin(), o = i, e = impl.end();
        while (i != e && from != to) {
            if (*i < *from) { *o = *i; ++i; ++o; }
            else if (*from < *i) { ++from; }
            else { ++i; ++result; }
        }
        o = std::copy(i, e, o);
        impl.erase(o, e);
        return result;
    }

    template <typename Iterator>
    void insertSorted(Iterator from, Iterator to) {
        auto pivot = impl.insert(impl.end(), from, to);
        std::inplace_merge(impl.begin(), pivot, impl.end());
        impl.erase(std::unique(impl.begin(), impl.end()), impl.end());
    }

    void merge(FlatSet const& rhs) {
        insertSorted(begin(rhs), end(rhs));
    }
};

#endif  // SUDOKUX_FLATSET_H
