#ifndef SUDOKUX_EXACT_COVER_SOLVER_H
#define SUDOKUX_EXACT_COVER_SOLVER_H

#include <vector>

#include "flat_set.h"

template <typename T>
struct Ignorable {
	Ignorable() = default;
	Ignorable(T t) : data(std::move(t)) {}

	mutable bool ignored = false;
	T data = {};
};

struct ExactCoverSolver {
	using Row = size_t;
	using Constraint = std::vector<Row>;
	using Column = Ignorable<std::vector<Row>>;
	using Matrix = std::vector<Column>;
	using Solution = std::vector<Row>;
	using IgnoredRows = FlatSet<Row>;  // TODO template-template parameter?

	ExactCoverSolver(Matrix matrix);

	static ExactCoverSolver fromConstraintMatrix(std::vector<Constraint> mat) {
		auto result = Matrix{};
		result.reserve(mat.size());
		result.insert(end(result), 
			std::make_move_iterator(begin(mat)), 
			std::make_move_iterator(end(mat)));
		return ExactCoverSolver(std::move(result));
	}

	Solution solve();

private:
	Matrix matrix = {};
	using ConstraintIterator = typename Matrix::const_iterator;
	std::vector<ConstraintIterator> chosenCols = {};
	std::vector<Row> chosenRows = {};
	std::vector<std::vector<ConstraintIterator>> colsToRewind = {};

	std::vector<std::vector<Row>> rowsToRewind = {};
	IgnoredRows sortedIgnoredRows = {};

	ConstraintIterator chooseColumn() const;

	void addChosenColum(ConstraintIterator colIt);

	using ColumnIterator = typename Constraint::const_iterator;

	ColumnIterator firstRow() const;
	ColumnIterator nextRow(Row row) const;

	bool canRewind() const { return ! colsToRewind.empty(); }
	Row rewind();
	void removeConstraintsAndRows(Row row);
};

template <typename Iterator0, typename Iterator1>
inline Iterator0 findFirstNotInSet(Iterator0 from0, 
	Iterator0 to0, 
	Iterator1 from1, 
	Iterator1 to1) {
	while (from0 != to0 && from1 != to1) {
		if (*from0 < *from1) { break; }
		else if (*from1 < *from0) { ++from1; }
		else { ++from0; }  // if the ranges are true sets, also increment from1
	}
	return from0;
}

#endif  // SUDOKUX_EXACT_COVER_SOLVER_H
