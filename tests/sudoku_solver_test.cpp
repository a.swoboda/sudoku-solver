#include <sstream>

#include <iostream>

#include "AlgorithmX.h"
#include "SudokuXConstraints.h"

int main() {
    // read sudoku
    auto stream = std::istringstream{
        "..48...17\n"
        "67.9.....\n"
        "5.8.3...4\n"
        "3..74.1..\n"
        ".69...78.\n"
        "..1.69..5\n"
        "1...8.3.6\n"
        ".....6.91\n"
        "24...15..\n"
    };
    // solve sudoku
    auto const initialRows = readInitialSudoku(stream);
    auto constraints = setupConstraints(initialRows);
    auto solver = AlgorithmX(constraints);
    auto const rc = solver.solve();
    if (not rc) {
        throw std::runtime_error("failed to solve");
    }
    auto oss = std::ostringstream{};
    auto completeSolution = initialRows;
    for (auto row : solver.solution()) {
        completeSolution.emplace_back(row);
    }
    outputSudoku(completeSolution, oss);
    // check result
    if ("934825617\n"
        "672914853\n"
        "518637924\n"
        "325748169\n"
        "469153782\n"
        "781269435\n"
        "197582346\n"
        "853476291\n"
        "246391578\n" != oss.str()) {
            std::cout << oss.str();
            throw std::runtime_error("incorrect solution");
    }
}
