Exact-cover solver (Knuth's Algorithm X), with an example for solving sudokus.

Make sure to read the Wikipedia entry

    https://en.wikipedia.org/wiki/Knuth%27s_Algorithm_X

if you want to know more about the algorithm.
